﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;

namespace WpfGetSystemScale
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void SetProperty<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return;

            storage = value;
            RaisePropertyChanged(propertyName);
        }
        #endregion

        private double _scaleX;
        private double _scaleY;

        public MainWindowViewModel()
        {
            RefreshCommand = new SimpleCommand(() =>
            {
                PresentationSource source = PresentationSource.FromVisual(App.Current.MainWindow);
                ScaleX = source.CompositionTarget.TransformToDevice.M11;
                ScaleY = source.CompositionTarget.TransformToDevice.M22;
            });
        }

        public double ScaleX
        {
            get => _scaleX;
            set => SetProperty(ref _scaleX, value);
        }
        public double ScaleY
        {
            get => _scaleY;
            set => SetProperty(ref _scaleY, value);
        }

        public ICommand RefreshCommand { get; }
    }

    public class SimpleCommand : ICommand
    {
        public SimpleCommand(Action action)
        {
            SimpleAction = action;
        }

        public Action SimpleAction { get; }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            SimpleAction();
        }
    }
}
