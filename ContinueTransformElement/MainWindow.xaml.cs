﻿using System.Windows;
using System.Windows.Media;

namespace WpfTest1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly TransformGroup _transformGroup;
        private readonly ScaleTransform _scaleTransform;
        private readonly TranslateTransform _translateTransform;

        public MainWindow()
        {
            InitializeComponent();
            _transformGroup = Rectangle.RenderTransform as TransformGroup;
            _translateTransform = _transformGroup.Children[0] as TranslateTransform;
            _scaleTransform = _transformGroup.Children[1] as ScaleTransform;
        }
        public bool _CarryOn { get; set; }
        public double _ScaleX { get; set; } = 2.5;
        public double _ScaleY { get; set; } = 1.5;
        public double _CenterX { get; set; } = 100;
        public double _CenterY { get; set; } = 100;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var transform = new TransformGroup();

            if (_CarryOn)
            {
                //如果要每次变化都在之前变化的结果之上继续变化，则行至本行代码； 否则每次都是从原始图形变化。
                transform.Children.Add(Rectangle.RenderTransform);
            }
            var transPoint = transform.Transform(new Point(_CenterX, _CenterY));
            transform.Children.Add(new ScaleTransform(_ScaleX, _ScaleY, transPoint.X, transPoint.Y));
            var mm = transform.Value;

            _scaleTransform.ScaleX = mm.M11;
            _scaleTransform.ScaleY = mm.M22;

            _translateTransform.X = mm.OffsetX / mm.M11;
            _translateTransform.Y = mm.OffsetY / mm.M22;
        }
    }
}
